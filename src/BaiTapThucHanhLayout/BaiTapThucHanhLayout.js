import React, { Component } from 'react'
import Banner from './Banner'
import Footer from './Footer'
import Header from './Header'
import Item from './Item'

export default class BaiTapThucHanhLayout extends Component {
  render() {
    return (
      <div>
        <div><Header/></div>
        <div className='container py-5'>
            <Banner/>
        </div>
        <div className='container'>
            <div className='row'>
                <div className='col-4'><Item/></div>
                <div className='col-4'><Item/></div>
                <div className='col-4'><Item/></div>
                <div className='col-4'><Item/></div>
                <div className='col-4'><Item/></div>
                <div className='col-4'><Item/></div>
            </div>
        </div>
            <div><Footer/></div>
      </div>
    )
  }
}
